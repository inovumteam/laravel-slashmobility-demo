<?php

namespace Tests\Unit;

use App\Product;
use App\Provider;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test logged user can access user data by id
     *
     * @return void
     */
    public function testLoggedUserCanGetUserById()
    {
        parent::setUp();

        $user = factory(User::class)->create();
        $createdUser = $user->getAttributes();
        unset($createdUser['password']);
        unset($createdUser['activation_token']);
        unset($createdUser['remember_token']);
        $this->actingAs($user, 'api')
            ->get('/api/users/' . $user->id)
            ->assertStatus(200)
            ->assertJsonFragment($createdUser);

    }
    /**
     * Test not logged user can't access users list
     *
     * @return void
     */
    public function testNotLoggedUserCantAccessUsersList()
    {
        parent::setUp();
        $headers = [
            'Accept' => 'application/json',
        ];
        factory(User::class, 10)->create();
        $expectedJson = [
            "message" => "Unauthenticated.",
        ];
        $this->get('/api/users/', $headers)
            ->assertStatus(401)
            ->assertJsonFragment($expectedJson)
            ->decodeResponseJson();
    }

    /**
     * Test user can log in
     *
     * @return void
     */
    public function testUserCanLogIn()
    {
        parent::setUp();

        $this->artisan('passport:install');

        $user = factory(User::class)->create([
            'active' => 1,
            'activation_token' => '',
        ]);

        $data = array(
            'email' => $user->email,
            'password' => 'password',
            'remember_me' => true,

        );

        $headers = [
            'Accept' => 'application/json',
        ];
        $expectedJsonStructure = [
            'access_token',
            'token_type',
            'expires_at',
        ];
        $this->post('/api/auth/login', $data, $headers)
            ->assertStatus(200)
            ->assertJsonStructure($expectedJsonStructure);
    }

    /**
     * Test user can create product
     *
     * @return void
     */
    public function testUserCanCreateProduct()
    {
        parent::setUp();

        $user = factory(User::class)->create([
            'active' => 1,
            'activation_token' => '',
        ]);

        $provider = factory(Provider::class)->create();

        $product = array(
            'name' => 'test product',
            'type' => 'test type',
            'description' => 'lorem ipsum',
            'image' => 'test.jpg',
            'provider_name' => $provider->name,
        );

        $headers = [
            'Accept' => 'application/json',
        ];

        $expectedJson = array('message' => 'Product created');

        $this->actingAs($user, 'api')
            ->post('/api/products/', $product, $headers)
            ->assertStatus(200)
            ->assertJsonFragment($expectedJson)
            ->decodeResponseJson();
    }

    /**
     * Test user can update product
     *
     * @return void
     */
    public function testUserCanUpdateProduct()
    {
        parent::setUp();

        $user = factory(User::class)->create([
            'active' => 1,
            'activation_token' => '',
        ]);

        $provider = factory(Provider::class)->create();

        $product = factory(Product::class)->create([
            'provider_name' => $provider->name,
        ]);

        $productValuesToUpdate = array(
            'name' => 'new test product name',
            'type' => 'new test type',
        );

        $headers = [
            'Accept' => 'application/json',
        ];

        $expectedJson = array('message' => 'Product updated');

        $this->actingAs($user, 'api')
            ->patch('/api/products/' . $product->id, $productValuesToUpdate, $headers)
            ->assertStatus(200)
            ->assertJsonFragment($expectedJson)
            ->decodeResponseJson();

        $updatedProduct = \App\Product::where('id', '=', $product->id)->first();

        $this->assertEquals($updatedProduct->name, $productValuesToUpdate['name']);
        $this->assertEquals($updatedProduct->type, $productValuesToUpdate['type']);
    }

    /**
     * Test user can create provider
     *
     * @return void
     */
    public function testUserCanCreateProvider()
    {
        parent::setUp();

        $user = factory(User::class)->create([
            'active' => 1,
            'activation_token' => '',
        ]);

        $provider = array(
            'name' => 'test provider',
            'address' => 'test address',
            'telephone' => '356123890',
            'city' => 'Barcelona',
        );

        $headers = [
            'Accept' => 'application/json',
        ];

        $expectedJson = array('message' => 'Provider created');

        $this->actingAs($user, 'api')
            ->post('/api/providers/', $provider, $headers)
            ->assertStatus(200)
            ->assertJsonFragment($expectedJson)
            ->decodeResponseJson();
    }

}
