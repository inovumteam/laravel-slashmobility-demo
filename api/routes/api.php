<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::get('signup/activate/{token}', 'AuthController@signupActivate');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'AuthController@logout');
    });
});
Route::group(['prefix' => 'password', 'namespace' => 'Auth'], function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('create', 'PasswordResetController@create');
        Route::get('find/{token}', 'PasswordResetController@find');
        Route::post('reset', 'PasswordResetController@reset');
    });
});

Route::group(['prefix' => 'users'], function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('current', 'AuthController@user');
        Route::get('', 'UserController@list');
        Route::get('{id}', 'UserController@getById');
        Route::get('username/{username}', 'UserController@getByUsername');
        Route::patch('', 'UserController@update');
    });
});

Route::group(['prefix' => 'providers'], function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('', 'ProviderController@create');
        Route::patch('{name}', 'ProviderController@update');
        Route::get('', 'ProviderController@list');
        Route::get('{name}', 'ProviderController@getByName');
    });
});

Route::group(['prefix' => 'products'], function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('', 'ProductController@create');
        Route::patch('{id}', 'ProductController@update');
        Route::get('', 'ProductController@list');
        Route::get('city/{city}', 'ProductController@getByCity');
        Route::get('type/{type}', 'ProductController@getByType');
        Route::get('{id}', 'ProductController@getById');
    });
});

Route::group(['prefix' => 'logs'], function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('', 'LogController@list');
        Route::get('level/{level}', 'LogController@getByLevel');
    });
});
