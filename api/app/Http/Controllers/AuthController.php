<?php
namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Notifications\SignupActivate;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AuthController extends Controller
{

    /**
     *   @OAS\SecurityScheme(
     *   securityScheme="bearerAuth",
     *   type="http",
     *   scheme="bearer"
     *   )
     **/

    /**
     * @OA\Post(
     *     path="/api/auth/signup",
     *     tags={"user"},
     *     summary="Register user",
     *     description="Registers new user",
     *     operationId="signup",
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="The user email for signup",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="The user password for signup",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password_confirmation",
     *         in="query",
     *         description="The user password confirmation for signup",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="username",
     *         in="query",
     *         description="The user username for signup",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="The user name for signup",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="lastname",
     *         in="query",
     *         description="The user last name for signup",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Successful user register",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function signup(UserRegisterRequest $request)
    {
        $request->validated();
        $user = new User([
            'username' => $request->username,
            'name' => $request->name,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'activation_token' => Str::random(40),
        ]);
        $user->save();
        Log::info("Successfully registred user with email " . $user->email);
        $user->notify(new SignupActivate($user));
        return response()->json([
            'message' => 'Successfully registred'], 201);
    }

    /**
     * @OA\Get(
     *     path="/api/auth/signup/activate/{token}",
     *     tags={"user"},
     *     summary="Activate user",
     *     description="Activates a new user by email token confirmation",
     *     operationId="signupActivate",
     *     @OA\Parameter(
     *         name="token",
     *         in="path",
     *         description="The token for user email confirmation",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful user activation",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Invalid token",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json(['message' => 'Invalid activation token'], 404);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->save();
        Log::info("Successfully validated user with email " . $user->email);
        return $user;
    }

    /**
     * @OA\Post(
     *     path="/api/auth/login",
     *     tags={"user"},
     *     summary="Logs user into system",
     *     operationId="login",
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="The user email for login",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="The user password for login",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="remember_me",
     *         in="query",
     *         description="Defines whether to remember user or not",
     *         required=true,
     *         @OA\Schema(
     *            enum={0, 1},
     *            type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful login",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function login(UserLoginRequest $request)
    {
        $request->validated();
        $credentials = request(['email', 'password']);
        $credentials['active'] = 1;
        $credentials['deleted_at'] = null;

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'], 401);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();
        Log::info("User with email " . $user->email ." logged into system");
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at)
                ->toDateTimeString(),
        ]);
    }

    /**
     * @OA\Get(
     *     path="/api/auth/logout",
     *     tags={"user"},
     *     summary="Activate user",
     *     description="Activates a new user by email token confirmation",
     *     operationId="signupActivate",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Successful logout",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        Log::info("User with email " . $request->user()->email ." logged out");
        return response()->json(['message' =>
            'Successfully logged out']);
    }

    /**
     * @OA\Get(
     *     path="/api/users/current",
     *     tags={"user"},
     *     summary="Gets current logged user",
     *     description="Gets current logged user information",
     *     operationId="user",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="User data",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function user(Request $request)
    {
        Log::info("User with email " . $request->user()->email ." requested current logged user information");
        return response()->json($request->user());
    }
}
