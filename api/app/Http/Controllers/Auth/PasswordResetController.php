<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordResetPerformRequest;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/password/create",
     *     tags={"password"},
     *     summary="Create password reset request",
     *     description="Creates password reset request and sends it to email",
     *     operationId="create",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Successful password reset creation",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function create()
    {
        $user = Auth::user();
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(50),
            ]
        );
        if ($user && $passwordReset) {
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
            Log::info("User with email " . $user->email . " created password reset request");

        }

        return response()->json([
            'message' => 'Change password request accepted. Check your mailbox.',
        ]);
    }

    /**
     * @OA\Get(
     *     path="/api/password/find/{token}",
     *     tags={"password"},
     *     summary="Verify password change request token",
     *     description="Verifies password change request token",
     *     operationId="find",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="token",
     *          in="path",
     *          description="Password change token",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Password token is correct",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function find($token)
    {
        // find password reset token or return error
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset) {
            return response()->json([
                'message' => 'This password reset token is invalid.',
            ], 404);
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.',
            ], 404);
        }
        return response()->json($passwordReset);
    }

    /**
     * @OA\Post(
     *     path="/api/password/reset",
     *     tags={"password"},
     *     summary="Reset password",
     *     description="Verifies token and updates user password with the new one",
     *     operationId="reset",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="token",
     *         in="query",
     *         description="The user password change token",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="The user email",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="New user password",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password_confirmation",
     *         in="query",
     *         description="New user password confirmation",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful password update",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function reset(PasswordResetPerformRequest $request)
    {
           $request->validated();
        $passwordReset = PasswordReset::where('token', $request->token)->first();
        $user = Auth::user();
        if (!$passwordReset || $passwordReset->email != $user->email) {
            return response()->json([
                'message' => 'This password reset token is invalid.',
            ], 404);
        }
        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        Log::info("User with email " . $user->email . " successfully changed his/her password");
        return response()->json($user);
    }
}
