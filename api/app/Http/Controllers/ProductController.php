<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Product;
use App\Provider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/products",
     *     tags={"products"},
     *     operationId="list",
     *     summary="Show products",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Successful products listing",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    function list() {
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " requested products list");
        return Product::all();
    }

    /**
     * @OA\Get(
     *     path="/api/products/type/{type}",
     *     tags={"products"},
     *     summary="Get products by type",
     *     description="Gets products information by product type",
     *     operationId="getByType",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="type",
     *          in="path",
     *          description="Product type",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful product listing by required type",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function getByType($type)
    {
        $products = Product::where('type', $type)->get();
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " requested products list with type " . $type);
        return response()->json($products);
    }

    /**
     * @OA\Get(
     *     path="/api/products/city/{city}",
     *     tags={"products"},
     *     summary="Get products by city",
     *     description="Gets products information by products provider city",
     *     operationId="getByCity",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="city",
     *          in="path",
     *          description="Product provider city",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful products listing by city",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function getByCity($city)
    {
        $products = [];
        $providers = Provider::where('city', $city)->get();
        foreach ($providers as $provider) {
            $productsByProvider = Product::where('provider_name', $provider->name)->get();
            foreach ($productsByProvider as $product) {
                $products[] = $product;
            }
        }
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " requested products list with city " . $city);

        return response()->json($products);
    }

    /**
     * @OA\Get(
     *     path="/api/products/{id}",
     *     tags={"products"},
     *     summary="Get products by id",
     *     description="Gets products information by product id",
     *     operationId="getById",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="Product id",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Product information by required id",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function getById($id)
    {
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " requested product data with id " . $id);
        $product = Product::find($id);
        return response()->json($product);
    }

    /**
     * @OA\Post(
     *     path="/api/products",
     *     tags={"products"},
     *     summary="Create product",
     *     description="Creates product with given information",
     *     operationId="create",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="The product name",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="The product type",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="description",
     *         in="query",
     *         description="The product description",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="image",
     *         in="query",
     *         description="The product image",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="provider_name",
     *         in="query",
     *         description="The product provider name",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Successful product creation",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Invalid provider name",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function create(ProductCreateRequest $request)
    {
        $request->validated();
        // find provider name or return error
        $provider = Provider::where('name', $request->provider_name)
            ->first();
        if (!$provider) {
            return response()->json([
                'message' => 'Provider with name: ' . $request->provider_name . ' doesn\'t exist, please use a real provider name.',
            ], 404);
        }

        $product = Product::updateOrCreate(
            [
                'name' => $request->name,
                'type' => $request->type,
                'description' => $request->description,
                'image' => $request->image,
                'provider_name' => $request->provider_name,
            ]
        );
        $product->save();
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " created product with name " . $request->name);
        return response()->json([
            'message' => 'Product created',
        ]);
    }

    /**
     * @OA\Patch(
     *     path="/api/products/{id}",
     *     tags={"products"},
     *     summary="Update product",
     *     description="Update product with given information",
     *     operationId="update",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The product id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="The product type",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="description",
     *         in="query",
     *         description="The product description",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="image",
     *         in="query",
     *         description="The product image",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="provider_name",
     *         in="query",
     *         description="The product provider name",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Successful product update",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Invalid provider name",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        $request->validated();
        $product = Product::find($id);
        if (!$product) {
            return response()->json([
                'message' => 'Product with id: ' . $id . ' doesn\'t exist',
            ], 404);
        }
        if ($request->provider_name) {
            $provider = Provider::where('name', $request->provider_name)
                ->first();
            if (!$provider) {
                return response()->json([
                    'message' => 'Provider with name: ' . $request->provider_name . ' doesn\'t exist, please use a real provider name.',
                ], 404);
            }
        }
        $product->update($request->all());
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " updated product with id " . $product->id);

        return response()->json([
            'message' => 'Product updated',
        ]);
    }

}
