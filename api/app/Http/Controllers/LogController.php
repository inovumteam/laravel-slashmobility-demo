<?php

namespace App\Http\Controllers;

use danielme85\LaravelLogToDB\LogToDB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LogController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/logs",
     *     tags={"logs"},
     *     operationId="list",
     *     summary="Show logs",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Successful logs listing",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    function list() {
        $logs = LogToDB::model()->get();
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " requested log list");
        return LogToDB::model()->get();
    }

    /**
     * @OA\Get(
     *     path="/api/logs/level/{level}",
     *     tags={"logs"},
     *     summary="Get logs by level",
     *     description="Gets logs by log level",
     *     operationId="getByType",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="level",
     *          in="path",
     *          description="Log level",
     *          required=true,
     *          @OA\Schema(
     *            enum={"INFO", "DEBUG", "NOTICE", "WARNING", "ERROR", "ALERT", "EMERGENCY"},
     *            type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful log listing by required log level",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function getByLevel($level)
    {
        $logs = LogToDB::model()->where("level_name", "=", $level)->get();
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " requested log list with level " . $level);
        return response()->json($logs);
    }

}
