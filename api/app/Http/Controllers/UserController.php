<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/users",
     *     tags={"user"},
     *     summary="Show users",
     *     operationId="list",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Successful users listing",
     *         @OA\JsonContent()
     *     ),
     *      @OA\Response(
     *          response="401",
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    function list() {
        $user = Auth::user();
        Log::info("User with email " . $user->email . " requested users list");
        return User::all();
    }

    /**
     * @OA\Patch(
     *      path="/api/users",
     *     tags={"user"},
     *      summary="Update logged user information",
     *      description="Updates logged user information",
     *      security={{"bearerAuth":{}}},
     *      operationId="update",
     *      @OA\Parameter(
     *          name="username",
     *          in="query",
     *          description="User username",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          description="User name",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="lastname",
     *          in="query",
     *          description="User lastname",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful user update",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response="401",
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response="default",
     *          description="An error occured",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function update(UserUpdateRequest $request)
    {
        $user = Auth::user();
        $request->validated();
        $user->update($request->all());
        Log::info("User with email " . $user->email . " update his/her profile");
        return response()->json([
            'message' => 'User updated',
        ]);
    }

    /**
     * @OA\Get(
     *     path="/api/users/username/{username}",
     *     tags={"user"},
     *     summary="Get user by username",
     *     description="Gets user information by username",
     *     operationId="getByUsername",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="username",
     *         in="path",
     *         description="User username",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User information by required username",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function getByUsername($username)
    {
        $user = User::where('username', $username)->first();
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " requested user data with username " . $username);
        return response()->json($user);
    }

    /**
     * @OA\Get(
     *     path="/api/users/{id}",
     *     tags={"user"},
     *     summary="Get user by id",
     *     description="Gets user information by user id",
     *     operationId="getById",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="User id",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User information by id",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function getById($id)
    {
        $user = User::find($id);
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " requested user data with id " . $id);
        return response()->json($user);
    }

}
