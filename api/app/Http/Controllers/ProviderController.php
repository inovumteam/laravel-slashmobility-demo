<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProviderCreateRequest;
use App\Http\Requests\ProviderUpdateRequest;
use App\Provider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProviderController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/providers",
     *     tags={"providers"},
     *     operationId="list",
     *     summary="Show providers",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Successful providers listing",
     *         @OA\JsonContent()
     *     ),
     *      @OA\Response(
     *          response="401",
     *          description="Unauthenticated",
     *          @OA\JsonContent()
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    function list() {
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " requested providers list");
        return Provider::all();
    }

    /**
     * @OA\Get(
     *     path="/api/providers/{name}",
     *     tags={"providers"},
     *     summary="Get provider by name",
     *     description="Gets provider information by provider name",
     *     operationId="getByName",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="name",
     *          in="path",
     *          description="Provider name",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Provider information by name",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function getByName($name)
    {
        $provider = Provider::where('name', $name)->first();
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " requested provider data with name " . $name);
        return response()->json($provider);
    }

    /**
     * @OA\Post(
     *     path="/api/providers",
     *     tags={"providers"},
     *     summary="Create provider",
     *     description="Creates provider with given information",
     *     operationId="create",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="The provider name",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="address",
     *         in="query",
     *         description="The provider address",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="telephone",
     *         in="query",
     *         description="The provider telephone",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="city",
     *         in="query",
     *         description="The provider city",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Successful provider creation",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function create(ProviderCreateRequest $request)
    {
        $request->validated();
        $provider = Provider::updateOrCreate(
            [
                'name' => $request->name,
                'address' => $request->address,
                'telephone' => $request->telephone,
                'city' => $request->city,
            ]
        );
        $provider->save();
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " created provider with name " . $request->name);
        return response()->json([
            'message' => 'Provider created',
        ]);
    }

    /**
     * @OA\Patch(
     *     path="/api/providers/{name}",
     *     tags={"providers"},
     *     summary="Update provider",
     *     description="Update provider with given information",
     *     operationId="update",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="The provider name",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="address",
     *         in="query",
     *         description="The provider address",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="telephone",
     *         in="query",
     *         description="The provider telephone",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="city",
     *         in="query",
     *         description="The provider city",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Successful provider update",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="An error occured",
     *         @OA\JsonContent()
     *     )
     * )
     */
    public function update(ProviderUpdateRequest $request, $name)
    {
        unset($request['name']);
        $request->validated();
        $provider = Provider::where('name', $name)->first();
        if (!$provider) {
            return response()->json([
                'message' => 'Provider with name: ' . $request->name . ' doesn\'t exist',
            ], 404);
        }
        $provider->update($request->all());
        $loggedUser = Auth::user();
        Log::info("User with email " . $loggedUser->email . " updated provider with name " . $request->name);
        return response()->json([
            'message' => 'Provider updated',
        ]);
    }
}
