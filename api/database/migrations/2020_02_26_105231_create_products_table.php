<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('name');
            $table->string('type');
            $table->string('description');
            $table->string('image');
            $table->string('provider_name');
            $table->timestamps();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('provider_name')
                ->references('name')->on('providers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
