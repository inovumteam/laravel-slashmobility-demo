<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\Provider;
use Faker\Generator as Faker;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */


$factory->define(Product::class, function (Faker $faker) {
    $providers = Provider::all()->pluck('name')->toArray();
    return [
        'name' => $faker->word,
        'type' => $faker->userName,
        'description' => $faker->sentence,
        'image' => $faker->imageUrl($width = 640, $height = 480),
        'provider_name' => $faker->randomElement($providers)
    ];
});
