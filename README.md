## Project configuration
1. Configure your database credentials in .env file
2. Use `composer install` to install all needed dependencies.
3. Use `php artisan migrate` command to create the tables.
4. Use `php artisan passport:install` command to create the encryption keys needed to generate secure access tokens.
5. Generate fake data and test your database:

- `php artisan tinker`

- `factory(App\User::class, 10)->create();`

- `factory(App\Provider::class, 10)->create();`

- `factory(App\Product::class, 10)->create();`
6. php artisan serve
Go to http://127.0.0.1:8000/api/documentation/ to use Swagger UI and test the API.

All the users generated with factories by default have password 'password'.


## Run project using Docker
1. Use `sh compose-services-up.sh` to start Docker containers.
2. Once all docker containers are running run command  `php artisan migrate` to create the tables.
3. Then run `php artisan passport:install` command to create the encryption keys.
4. Now you can access the API from http://127.0.0.1:3000
5. Go to http://127.0.0.1:3000/api/documentation/ to use Swagger UI and test the API.

## Tests
1. `./vendor/bin/phpunit` to run tests
2. On March 2020 the app is deployed for one month on https://www.inovum-solutions.com/slashmobility-api/api/documentation


